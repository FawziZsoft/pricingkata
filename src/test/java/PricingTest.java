import buyingoperation.BuyByUnity;
import buyingoperation.BuyingByWeight;
import buyingoperation.BuyingSystem;
import buyingoperation.BuyingTypes;
import dto.ItemWithQuantity;
import entites.Item;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pricing.CalculatePrice;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class PricingTest {

    @Test
    void should_return_true(){
        Assertions.assertTrue(true);
    }

    @Test
    void should_return_the_total_price_of_a_bought_item(){
        //Given
        Item item =  Item.builder().name("X").price(2.0).build();
        ItemWithQuantity itemWithQuantity = new ItemWithQuantity(item,2.0);

        Item item2 = Item.builder().name("XX").price(3.0).build();
        ItemWithQuantity itemWithQuantity2 = new ItemWithQuantity(item2,3.0);

        Set<ItemWithQuantity> itemWithQuantities = new HashSet(Arrays.asList(itemWithQuantity, itemWithQuantity2));
        BuyingTypes calculus = new BuyByUnity();
        //When
        double res = calculus.buyUnity(itemWithQuantities);
        //Then
        Assertions.assertEquals(res,13);
    }

    @Test
    void should_return_total_price_according_to_weight(){

        //Given
        Item item =  Item.builder().name("X").price(100.0).build();
        ItemWithQuantity itemWithQuantity = new ItemWithQuantity(item,3.2);

        Set<ItemWithQuantity> itemWithQuantities = new HashSet(Arrays.asList(itemWithQuantity));
        BuyingTypes calculus = new BuyingByWeight();
        //When
        double res = calculus.buyWeight(itemWithQuantities);
        //Then
        double expectedTotal = (3.2 * 100) / 16;
        Assertions.assertEquals(res,expectedTotal);

    }

    @Test
    void should_return_total_price_of_weight_or_unity_item(){

        Item item =  Item.builder().name("X").price(100.0).build();
        ItemWithQuantity itemWithQuantity = new ItemWithQuantity(item,3.2);

        Set<ItemWithQuantity> itemWithQuantities = new HashSet(Arrays.asList(itemWithQuantity));
        double expectedTotal = (3.2 * 100) / 16;

        BuyingSystem buyingSystem = new BuyingSystem();
        double res = buyingSystem.buy("unit",itemWithQuantities);
        double res1 = buyingSystem.buy("weight",itemWithQuantities);


        Assertions.assertEquals(res,320);
        Assertions.assertEquals(res1,20);



    }
}
