package entites;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;


@Getter
@Setter
@Builder
public class Item {
    private final String name;
    private final Double price;


    public class builder extends Item {
        builder(String name, Double price) {
            super(name, price);
        }
    }
}
