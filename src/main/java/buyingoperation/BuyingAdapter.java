package buyingoperation;

import dto.ItemWithQuantity;

import java.util.Set;

public class BuyingAdapter implements Buying {

    BuyingTypes buyingTypes;


    public BuyingAdapter(String buyTypes){
        if(buyTypes.equalsIgnoreCase("Unit")){
            buyingTypes = new BuyByUnity();
        }
        else if (buyTypes.equalsIgnoreCase("Weight")){
            buyingTypes = new BuyingByWeight();
        }
    }

    @Override
    public double buy(String buyTypes, Set<ItemWithQuantity> itemWithQuantities) {
        double total = 0.0;

        if (buyTypes.equalsIgnoreCase("Unit")){
            total = buyingTypes.buyUnity(itemWithQuantities);
        }
        else if (buyTypes.equalsIgnoreCase("Weight")){
            total = buyingTypes.buyWeight(itemWithQuantities);
        }

        return total;
    }
}
