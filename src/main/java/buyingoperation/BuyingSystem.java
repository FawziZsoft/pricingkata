package buyingoperation;

import dto.ItemWithQuantity;

import java.util.Set;

public class BuyingSystem implements Buying {

    BuyingAdapter buyingAdapter;
    @Override
    public double buy(String buyingTypes, Set<ItemWithQuantity> itemWithQuantities) {
        double res = 0.0;

        if (buyingTypes.equalsIgnoreCase("Unit")||
                buyingTypes.equalsIgnoreCase("Weight"))
        {
            buyingAdapter = new BuyingAdapter(buyingTypes);
            res = buyingAdapter.buy(buyingTypes,itemWithQuantities);
        }
        return res;

    }
}
