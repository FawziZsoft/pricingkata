package buyingoperation;

import dto.ItemWithQuantity;

import java.util.Set;

public interface BuyingTypes {
    public double buyUnity(Set<ItemWithQuantity> itemWithQuantities);
    public double buyWeight(Set<ItemWithQuantity> itemWithQuantities);
}
