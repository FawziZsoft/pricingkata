package buyingoperation;

import dto.ItemWithQuantity;

import java.util.Set;

public class BuyingByWeight implements BuyingTypes {
    @Override
    public double buyUnity(Set<ItemWithQuantity> itemWithQuantities) {
        return 0;
    }

    @Override
    public double buyWeight(Set<ItemWithQuantity> itemWithQuantities) {
        return itemWithQuantities.stream().map( itemWithQuantity-> itemWithQuantity.getItem().getPrice() *
                itemWithQuantity.getQuantity()/16).reduce(0d,(total,subtotal)-> total + subtotal);
    }
}
