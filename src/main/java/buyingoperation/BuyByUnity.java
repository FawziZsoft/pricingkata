package buyingoperation;

import dto.ItemWithQuantity;
import java.util.Set;

public class BuyByUnity implements BuyingTypes {

    @Override
    public double buyUnity(Set<ItemWithQuantity> itemWithQuantities) {
        return itemWithQuantities.stream().map( itemWithQuantity-> itemWithQuantity.getItem().getPrice() *
                    itemWithQuantity.getQuantity()).reduce(0d,(total,subtotal)-> total + subtotal);
        }

    @Override
    public double buyWeight(Set<ItemWithQuantity> itemWithQuantities) {
        return 0;
    }
}
