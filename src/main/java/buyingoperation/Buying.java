package buyingoperation;

import dto.ItemWithQuantity;

import java.util.Set;

public interface Buying {
    double buy(String buyingTypes, Set<ItemWithQuantity> itemWithQuantities);
}
