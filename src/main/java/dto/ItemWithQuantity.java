package dto;

import entites.Item;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;


@Getter
@Setter
@AllArgsConstructor
public class ItemWithQuantity {

    private Item item;
    private Double quantity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemWithQuantity that = (ItemWithQuantity) o;
        return Double.compare(that.quantity, quantity) == 0 &&
                item.equals(that.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, quantity);
    }
}
