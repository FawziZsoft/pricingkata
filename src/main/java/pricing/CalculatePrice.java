package pricing;

import dto.ItemWithQuantity;

import java.util.Set;

public class CalculatePrice {

    public double simpleCalculation(Set<ItemWithQuantity> itemWithQuantities){
        return itemWithQuantities.stream().map( itemWithQuantity-> itemWithQuantity.getItem().getPrice() *
                itemWithQuantity.getQuantity()).reduce(0d,(total,subtotal)-> total + subtotal);
    }
}
